import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../screens/home.screen.dart';
import '../screens/stopwatch.screen.dart';

class App extends StatelessWidget{
@override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      routes: {
        '/':(context) => HomeScreen(),
        '/stopwatch':(context)=>StopWatchScreen(),
      },
      initialRoute: '/'
    );
  }
}