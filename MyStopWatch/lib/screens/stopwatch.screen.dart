import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

class StopWatchScreen extends StatefulWidget{
 @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StopWatchScreenState();
  }
}


class StopWatchScreenState extends State<StopWatchScreen> {
  final StopWatchTimer _stopWatchTimer = StopWatchTimer();
  final _isHours = true;
  final _scrollController = ScrollController();


  @override
  void dispose() async {
    super.dispose();
    await _stopWatchTimer.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
          image: ExactAssetImage('assets/imgs/watch_background3.jpg'),
            fit: BoxFit.cover,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height:200),
            StreamBuilder<int>(
              stream: _stopWatchTimer.rawTime,
              initialData: _stopWatchTimer.rawTime.value,
              builder: (context,snapshot) {
                final value = snapshot.data;
                final displayTime = StopWatchTimer.getDisplayTime(value, hours: _isHours);
                return Text(displayTime,style: const TextStyle(fontSize: 40,fontWeight: FontWeight.bold),);
              }
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(
                    color: Colors.green,
                    onPress: () {
                      _stopWatchTimer.onExecute.add(StopWatchExecute.start);
                      },
                    label: 'Start'),
                SizedBox(width: 10),
                CustomButton(
                    color: Colors.red,
                    onPress: () {
                      _stopWatchTimer.onExecute.add(StopWatchExecute.stop);
                    },
                    label: 'Stop'),
              ],
            ),
            CustomButton(
                color: Colors.lightBlueAccent,
                onPress: () {
                  _stopWatchTimer.onExecute.add(StopWatchExecute.lap);
                },
                label: 'Lap'),
            SizedBox(width: 10),
            CustomButton(
                color: Colors.deepOrangeAccent,
                onPress: () {
                  _stopWatchTimer.onExecute.add(StopWatchExecute.reset);
                },
                label: 'Reset'),
            Container(
              height: 120,
              margin: const EdgeInsets.all(8),
              child: StreamBuilder<List<StopWatchRecord>>(
                stream: _stopWatchTimer.records,
                initialData: _stopWatchTimer.records.value,
                builder: (context, snap) {
                  final value = snap.data!;
                  if (value.isEmpty) {
                    return Container();
                  }
                  Future.delayed(const Duration(milliseconds: 100), () {
                    _scrollController.animateTo(
                        _scrollController.position.maxScrollExtent,
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.easeOut);
                  });
                  print('Listen records. $value');
                  return ListView.builder(
                    controller: _scrollController,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (BuildContext context, int index) {
                      final data = value[index];
                      print("Value: $data");
                      return Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: Text(
                              'Lap${index + 1}: ${data.displayTime}',
                              style: const TextStyle(
                                  fontSize: 17,
                                  fontFamily: 'Helvetica',
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                          const Divider(
                            height: 1,
                          )
                        ],
                      );
                    },
                    itemCount: value.length,
                  );
                },
              ),
            ),

          ],

        ),
      )
    );
  }
}

class CustomButton extends StatelessWidget {
  late final Color color;
  final Function onPress;
  final String label;

  CustomButton({required this.color,required this.onPress,required this.label});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return RaisedButton(
      padding: const EdgeInsets.all(4),
      color: color,
      shape: const StadiumBorder(),
      onPressed: ()=>{onPress()},
      child: Text(
        label,
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}

