import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_stop_watch/widgets/formInput.dart';
import '../widgets/iconfont.dart';
import '../validation/mixins_validation.dart';

class HomeScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}


class HomeScreenState extends State<HomeScreen> with CommonValidation{
  // final _formKey = GlobalKey<FormState>();
  String email = "";
  String password = "";
@override
  Widget build(BuildContext context) {
  // TODO: implement build
  return Scaffold(
    resizeToAvoidBottomInset: false,
    body: Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/imgs/watch_background3.jpg'),
          fit: BoxFit.cover,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0)),
                  Center(
                    child: ClipOval(
                      child: Container(
                        width: 120,
                        height: 120,
                        color: Colors.black87,
                        alignment: Alignment.center,
                        child: IconFont(iconName: "N", color: Colors.white,
                            size: 100
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                  Text('MyStopWatch',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 40,
                          fontWeight: FontWeight.bold
                      )
                  ),
                  SizedBox(height: 2),
                  FormInput(),
                ]
            ),
          )
        ],
      ),
    ),
  );
}


}