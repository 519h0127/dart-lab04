mixin CommonValidation{
  String? validateEmail(String? value){
    if(value == null || value.isEmpty){
      return "Please enter some text";
    } else if (!value.contains('@') || !value.contains('.')) {
      return 'Please enter valid email.';
    }
    return null ;
  }

  String? isPasswordValidated(String? password,[int minLength = 8]){
    if (password == null || password.isEmpty) {
      return  "Please enter your password";
    } else if(!password.contains(new RegExp(r'[A-Z]'))){
      return  "Password must have at least one uppercase letter";
    } else if(!password.contains(new RegExp(r'[a-z]'))){
      return  "Password must have at least one lowercase letter";
    }
    // else if(!password.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'))){
    //   return  "Password must have at least one special character";
    // }
    else if (password.length < minLength){
      return  "Password must have at least 8 characters";
    }
    return null;
  }
}