import 'package:flutter/material.dart';
import '../validation/mixins_validation.dart';

class FormInput extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return FormInputState();
  }

}

class FormInputState extends State<FormInput> with CommonValidation{
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String email = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        padding:EdgeInsets.all(20) ,
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(margin: EdgeInsets.only(top: 10.0),),
              EmailInput(),
              Container(margin: EdgeInsets.only(top: 10.0),),
              PasswordInput(),
              Container(margin: EdgeInsets.only(top: 10.0),),
              LoginButton()
            ],
          ),
        )
    );
  }

  Widget EmailInput() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        hintText: 'What do people call you?',
        labelText: 'Email *',
      ),
      validator: validateEmail,
      onSaved: (value) {
        email = value!;
      },
    );
  }

  Widget PasswordInput() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        hintText: 'This must secure?',
        labelText: 'Password *',
      ),
      validator: isPasswordValidated,
      onSaved: (value) {
        password = value!;
      },
    );
  }

  Widget LoginButton() {
    return ElevatedButton(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Processing Data')),
            );
            print(email);
            print(password);
            Navigator.of(context).pushReplacementNamed('/stopwatch',arguments: email);
          }
        },
        child: const Text("Login"));
  }

}