import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}


class HomePageState extends State<HomePage>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: const Text('Search App'),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  showSearch(context: context, delegate: DataSearch());
                },
                icon: const Icon(Icons.search)
            ),
          ],
        ),
        drawer: Drawer(),
    );
  }
}


class DataSearch extends SearchDelegate<String> {
  final cities = [
    "Mumbai",
    "Delhi",
    "Pune",
    "Indore",
    "Kanpur",
    "Agra",
    "Kolkata",
    "Howrah"
  ];
  static String searchItem="";
  final recentCities = ["Kanpur"];
  @override
  List<Widget>? buildActions(BuildContext context) {
    //action for app bar
    return [IconButton(
        onPressed: () {
          query = "";
        },
        icon: const Icon(Icons.clear))];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    //leading icon on the left of the appbar
    return IconButton(
        onPressed: () {
          close(context, "");
        },
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow,
            progress: transitionAnimation)
        );
  }

  @override
  Widget buildResults(BuildContext context) {
    //show some result\
    query = searchItem.toString();
    var isQueryExist = recentCities.where((element) => element.toLowerCase() == searchItem.toString().toLowerCase());
    if(isQueryExist.isEmpty){
      recentCities.add(searchItem.toString());
    }
    return Center(
      child: Container(
        height: 100,
        width: 100,
        child: Card(
          color: Colors.deepOrangeAccent,
          child: Center(
            child: Text(searchItem.toString()),
          ),
        ),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    //show when someone search for something
    final suggestionList = query.isEmpty ? recentCities : cities.where((element) => element.toLowerCase().startsWith(query)).toList();
    return ListView.builder(
        itemBuilder: (context, index) => ListTile(
          onTap: (){
            showResults(context);
            print(suggestionList[index]);
            searchItem = suggestionList[index];
          },
          leading: Icon(Icons.location_city),
          trailing: IconButton(icon: Icon(Icons.clear),onPressed:(){
            print(index);
            recentCities.removeAt(index);
          }),
          title: RichText(
            text: TextSpan(
              text: suggestionList[index].substring(0,query.length),
              style: TextStyle(
                  color: Colors.black,fontWeight: FontWeight.bold
              ),
                children: [
                  TextSpan(
                    text: suggestionList[index].substring(query.length),
                    style: TextStyle(color: Colors.grey)
                  )
                ]
            ),
          ),
        ),
        itemCount: suggestionList.length
    );
  }
}