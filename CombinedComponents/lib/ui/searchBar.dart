
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search App'),
        actions: <Widget>[
          IconButton(onPressed: (){}, icon: Icon(Icons.search)),
        ],
      ),
    );
  }
}
